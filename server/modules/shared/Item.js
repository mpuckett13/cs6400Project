(function() {
  'use strict';

  var Item = function(item) {
    this.itemName = item.itemName;
    this.initialPrice = item.initialPrice;
  };

  module.exports = Item;
})();