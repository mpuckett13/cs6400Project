(function() {
  'use strict';

  const dao = require('./rethinkDao.js');
  const service = require('../shared/sharedService.js')({ dao: dao });

  module.exports = {

    /**
     * Adds a new item
     * @param item {Object} - the item object to add
     * @param item.itemName {String} - the name of the item
     * @returns {Promise}
     */
    addItem: function addItem(item) {
      return service.addItem(item);
    },

    /**
     * Adds a new user
     * @param user {Object} - the user object to add
     * @param user.name {String} - the userName of the user. Must be unique
     * @returns {Promise}
     */
    addUser: function addUser(user) {
      return service.addUser(user);
    },

    /**
     * Configures a user to watch an item, and then gets the date/time when
     * the item goes on auction.
     * @param userName {String} - the user who is watching an item
     * @param itemName {String} - the item that will be watched
     * @returns {Promise} - resolves to the item
     */
    addUserWatch: function addUserWatch(userName, itemName) {
      return service.addUserWatch(userName, itemName);
    },

    /**
     * Gets a list of all items in the system
     * @returns {Promise} - resolves a list of items
     */
    getAllItems: function getAllItems() {
      return service.getItems();
    },

    /**
     * Gets a list of all users in the system
     * @returns {Promise} - resolves a list of users
     */
    getAllUsers: function getAllUsers() {
      return service.getUsers();
    },

    /**
     * Gets a single item by itemName
     * @param itemName {String} - the id of the item to get
     * @returns {Promise} - resolves to the item object
     */
    getItem: function getItem(itemName) {
      return service.getItem(itemName);
    },

    /**
     * Submits a bid. There is no guarantee that this bid will be accepted. If this bid loses a
     * race condition with another bid for the same item, it will simply be ignored.
     * @param userName {String} - the userName that is responsible for submitting the bid.
     * @param itemName {String} - the item that is being bid on
     * @param price {Number} - the price of the submitted bid.
     */
    submitBid: function(userName, itemName, price) {
      return service.submitBid(userName, itemName, price);
    },

    /**
     * Called from the simulation runner to kick off the simulation. This should be called after the simulation
     * has been set up (all users added, all items added, and all user watches added).
     * @returns {Promise} - resolves once all auctions have expired (by per-item configured time limit)
     */
    startSimulation: function() {
      return service.startSimulation();
    },

    /**
     * Clears out all cached simulation information
     */
    resetSimulation: function() {
      service.resetSimulation();
    }
  };
})();