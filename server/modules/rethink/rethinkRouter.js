(function() {
  'use strict';

  const express = require('express');

  const rethinkDao = require('./rethinkDao.js');

  const router = express.Router();

  /**
   * Function that abstracts common logic in DAO calls.
   * @param res {Object} - the express response object from the route handler
   * @param doaFunc {Function} - reference to the DAO function to call
   * @returns {Promise}
   */
  function handleGetAllRequest(res, daoFunc) {
    daoFunc()
        .then(result => {
          res.status(200).send(result);
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  }

  /**
   * Function that abstracts common logic in DAO calls.
   * @param res {Object} - the express response object from the route handler
   * @param doaFunc {Function} - reference to the DAO function to call
   * @param entity {Object} - the entity to add to the database
   * @returns {Promise}
   */
  function handlePostRequest(res, daoFunc, entity) {
    daoFunc(entity)
        .then(result => {
          res.status(201).send(result);
        })
        .catch(err => {
          console.error(err);
          res.status(500).end();
        });
  }

  router.get('/user', (req, res) => {
    handleGetAllRequest(res, rethinkDao.getUsers);
  });

  router.post('/user', (req, res) => {
    handlePostRequest(res, rethinkDao.addUser, req.body);
  });

  router.get('/item', (req, res) => {
    handleGetAllRequest(res, rethinkDao.getItems);
  });

  module.exports = router;
})();