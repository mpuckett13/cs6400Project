(function () {
  'use strict';

  const Promise = require('promise');
  const moment = require('moment');
  const User = require('../modules/shared/User.js');
  const Item = require('../modules/shared/Item.js');
  
  const postgresService = require('../modules/postgres/postgresService.js');
  const rethinkService = require('../modules/rethink/rethinkService.js');

  function runSimulation(service, initData) {

    const simUsers = {};
    const simItems = {};

    return new Promise((resolve, reject) => {

      var addUsersPromise = new Promise((resolve, reject) => {
        let initUsers = [];
        
        let i;
        for (i = 0; i < initData.numUsers; i++) {
          initUsers.push({ userName: ('user' + i.toString()) });
        }

        console.log(`Adding ${initData.numUsers} users...`);
        Promise.all(initUsers.map(service.addUser))
            .then(result => {

              result.forEach(user => {
                simUsers[user.userName]
                    = new User(user.userName, initData.maxBidDelay);
              });

              console.log(`Finished adding ${result.length} users.`);
              resolve();
            })
            .catch(err => {
              console.error('Error adding users: ', err);
              reject(err);
            });
      });

      var addItemsPromise = new Promise((resolve, reject) => {
        addUsersPromise
            .then(() => {
              let initItems = [];

              let i;
              for (i = 0; i < initData.numItems; i++) {
                initItems.push({
                  itemName: ('item' + i.toString()),
                  initialPrice: 1,
                  auctionTimeLimit: initData.auctionTimeLimit
                });
              }
              console.log(`Adding ${initData.numItems} items...`);
              Promise.all(initItems.map(service.addItem))
                .then(results => {

                  results.forEach(item => {
                    simItems[item.itemName] = new Item(item);
                  });

                  console.log(`Finished adding ${results.length} items.`);
                  resolve();
                })
                .catch(err => {
                  console.error('Error adding items: ', err);
                  reject(err);
                });
            })
            .catch(err => {
              reject(err);
            });
      });

      var watchItemsPromise = new Promise((resolve, reject) => {
        addItemsPromise
            .then(() => {
              console.log('Configuring users to watch items...');
              const promises = [];

              Object.keys(simUsers).forEach(userName => {
                Object.keys(simItems).forEach(itemName => {
                  promises.push(service.addUserWatch(simUsers[userName], itemName));
                });
              });

              Promise.all(promises)
                  .then(result => {
                    resolve();
                  })
                  .catch(err => {
                    console.error(
                      'Error configuring users watching items: ',
                      err
                    );
                    reject(err);
                  });
            })
            .catch(err => {
              reject(err);
            });
      });

      var simulationPromise = new Promise((resolve, reject) => {
        watchItemsPromise
            .then(() => {
              console.log('Starting item auctions...');

              service.startSimulation()
                  .then(result => {
                    resolve(result);
                  })
                  .catch(err => {
                    reject(err);
                  });
            })
            .catch(err => {
              reject(err);
            });
      });

      simulationPromise
          .then(result => {
            resolve(result);
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function triggerSimulation(simulationData) {
    const simResults = {
      numUsers: simulationData.numUsers,
      numItems: simulationData.numItems,
      maxBidDelay: simulationData.maxBidDelay,
      auctionTimeLimit: simulationData.auctionTimeLimit
    };
    
    return new Promise((resolve, reject) => {
      console.log('\nRunning simulation with PostgreSQL');
      runSimulation(postgresService, simulationData)
          .then(postgresResult => {

            simResults.postgres = postgresResult;
            
            console.log('\nRunning simulation with RethinkDB');
            runSimulation(rethinkService, simulationData)
                .then(rethinkResult => {

                  simResults.rethink = rethinkResult;

                  postgresService.resetSimulation();
                  rethinkService.resetSimulation();

                  resolve(simResults);
                });
          })
          .catch(err => {
            console.error(err);
            reject(err);
          });
    });
  }

  module.exports = {
    start: triggerSimulation
  }
})();