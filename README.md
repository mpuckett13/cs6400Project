# CS-6400 Project

## Useful Commands (all commands run from `${projectRoot}`)

---
#### Start all containers
```
docker-compose up
```

---
#### Run postgres rebuild script (postgres container must be running)
```
psql -h localhost -d ${databaseName} -U ${userName} -f postgres/scripts/rebuildDb.sql
```

---
#### Run rethink rebuild script (rethink container must be running)
```
node rethink/scripts/rebuildDb.js
```