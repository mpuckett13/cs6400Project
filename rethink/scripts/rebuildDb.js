(function() {
  'use strict';

  const r = require('rethinkdb');
  const Promise = require('promise');
  const moment = require('moment');

  const PROJECT_DB_NAME = 'project';

  const TABLES = {
    USER: { name: 'users', pk: 'userName' },
    ITEM: { name: 'items', pk: 'itemName' }
  };

  /**
   * Function that will create a new database table.
   * @param tableName {String} - the name of the table to create
   * @param dbName {String} - the name of the database to create the table in
   * @param conn {Object} - the RethinkDB database connection
   * @param pk {String} (optional) - field to use as the primary key
   */
  function createTable(tableName, dbName, conn, pk) {
    return new Promise((resolve, reject) => {
      
      var dbCall;
      if (pk) {
        dbCall = r.db(dbName).tableCreate(tableName, { primaryKey: pk });
      } else {
        dbCall = r.db(dbName).tableCreate(tableName);
      }

      dbCall
          .run(conn, (err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
    });
  }

  /**
   * Function that will drop a database (including all tables and data)
   * @param dbName {String} - the name of the database to drop
   * @param conn {Object} - the RethinkDB database connection
   */
  function dropDatabaseIfExists(dbName, conn) {
    return new Promise((resolve, reject) => {
      r.dbList()
          .run(conn, (err, databases) => {
            if (err) {
              return reject(err);
            }
            
            console.log('dbs', databases);

            console.log('dbName: ', dbName)
            if (databases.indexOf(dbName) < 0) {
              console.log('not found');
              return resolve();
            }

            r.dbDrop(dbName)
                .run(conn, (err, result) => {
                  if (err) {
                    return reject(err);
                  }
                  return resolve(result);
                })
          });
    });
  }

  function createDatabase(dbName, conn) {
    return new Promise((resolve, reject) => {
      r.dbCreate(dbName)
          .run(conn, (err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
    });
  }

  /**
   * Function that will insert data into a specified table.
   * @param tableName {String} - the name of the table to insert the data in
   * @param conn {Object} - the RethinkDB database connection
   * @param record {Object} - The data to insert into the table.
   */
  function insertData(tableName, conn, record) {
    return new Promise((resolve, reject) => {
      r.table(tableName)
          .insert(record)
          .run(conn, (err, result) => {
            if (err) {
              return reject(err);
            }
            return resolve(result);
          });
    });
  }

  function handleErr(err, conn) {
    console.log(err);
    if (conn) {
      conn.close();
    }
  }

  console.log('Connecting to database...');
  r.connect({ host: 'localhost', port: 28015, db: PROJECT_DB_NAME })
      .then(conn => {

        console.log('Connected to database');
        /*
        * Wrap database drop promises up into a single promise
        */
        var schemaDropPromise = new Promise((resolve, reject) => {
          
          console.log('Dropping database(s)...');

          var promises = [];

          promises.push(dropDatabaseIfExists(PROJECT_DB_NAME, conn));

          Promise
              .all(promises)
              .then(result => {
                console.log('Finished dropping database(s)');
                return resolve(result);
              })
              .catch(err => {
                handleErr(err, conn);
              });
        });

        var schemaCreatePromise = new Promise((resolve, reject) => {
          // don't start adding databases until all specified dbDrops have completed
          schemaDropPromise
            .then(result => {
              console.log('Creating database(s)...');

              var promises = [];

              promises.push(createDatabase(PROJECT_DB_NAME, conn));

              Promise
                  .all(promises)
                  .then(result => {
                    console.log('Finished adding database(s)');
                    return resolve(result);
                  })
                  .catch(err => {
                    handleErr(err, conn);
                  });
            })
            .catch(err => {
              handleErr(err, conn);
            });
        });

        /*
        * Wrap all table add promises up into a single promise.
        */
        var schemaBuildPromise = new Promise((resolve, reject) => {
          // don't start adding tables until all specified table drops have completed
          schemaCreatePromise
              .then(result => {

                console.log('Adding table(s)...');
                
                var promises = [];

                Object.keys(TABLES).forEach(tableKey => {
                  promises.push(createTable(TABLES[tableKey].name, PROJECT_DB_NAME, conn, TABLES[tableKey].pk));
                });

                Promise
                    .all(promises)
                    .then(result => {
                      console.log('Finished adding table(s)');
                      return resolve(result);
                    })
                    .catch(err => {
                      handleErr(err, conn);
                    });
              })
              .catch(err => {
                handleErr(err, conn);
              });
        });

        /*
        * Wrap all data insert promises up into a single promise
        */
        var dataInsertPromise = new Promise((resolve, reject) => {
          // don't start inserting data until all specified table adds have completed
          schemaBuildPromise
              .then(result => {

                // console.log('Inserting data...');
                var promises = [];

                // // populate the user table
                // promises.push(insertData(TABLES.USER.name, conn, [
                //   { userName: 'user1' },
                //   { userName: 'user2' },
                //   { userName: 'user3' }
                // ]));

                // promises.push(insertData(TABLES.ITEM.name, conn, [
                //   {
                //     name: 'item1',
                //     maxBid: {
                //       userName: 'user2',
                //       price: 1.10
                //     },
                //     bids: [
                //       { userName: 'user2', price: 1.10 },
                //       { userName: 'user1', price: 1.00 }
                //     ]
                //   },
                //   {
                //     name: 'item2'
                //   },
                //   {
                //     name: 'item3'
                //   }
                // ]));

                Promise
                    .all(promises)
                    .then(result => {
                      // console.log('Finished inserting data');
                      return resolve(result);
                    })
                    .catch(err => {
                      handleErr(err, conn);
                    });
              })
              .catch(err => {
                handleErr(err, conn);
              });
        });

        // Once all data insertions have completed, go ahead and close the database connection
        dataInsertPromise
            .then(result => {
              console.log('All actions completed successfully...closing connection.');
              conn.close();
            })
            .catch(err => {
              handleErr(err, conn);
            });
      })
      .catch(err => {
        throw err;
      });
})();
